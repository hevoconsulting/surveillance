package com.surveillance.bean;


import com.surveillance.persistence.Test;
import com.surveillance.services.TestService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.stream.Stream;

@Singleton
@Startup
public class StartupBean {
    private final TestService testService;

    @Inject
    public StartupBean(TestService testService) {
        this.testService = testService;
    }

    @PostConstruct
    private void startup() {
        /*Stream.of(7
                , 11
                , 15)
                .forEach(id ->
                        testService.addId(new Test(id))
                );*/
        testService.getAllTests().forEach(System.out::println);
    }

    @PreDestroy
    private void shutdown() {
        testService.clear();
    }
}
