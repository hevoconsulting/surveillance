package com.surveillance.persistence;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Test {
    @Id
    private Integer id;

    public Test(Integer id) {
        this.id = id;
    }

    @Override
    public String toString(){
        return "Test = {" + id + "}";
    }
}
