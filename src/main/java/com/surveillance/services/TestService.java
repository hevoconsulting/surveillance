package com.surveillance.services;


import com.surveillance.persistence.Test;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

@Stateless
public class TestService {
    @PersistenceContext(unitName = "test-pu")
    private EntityManager entityManager;

    public void addId(Test test) {
        entityManager.persist(test);
    }

    public List<Test> getAllTests() {
        CriteriaQuery<Test> cq = entityManager.getCriteriaBuilder().createQuery(Test.class);
        cq.select(cq.from(Test.class));
        return entityManager.createQuery(cq).getResultList();
    }

    public void clear() {
    }
}
